(require 'ert)
(require 'computer-whiz)

(ert-deftest ert-matching-characters ()
  (should (= (computer-whiz--matching-characters "aaa" "aaa") 3))
  (should (= (computer-whiz--matching-characters "abc" "def") 0))
  (should (= (computer-whiz--matching-characters "abc" "dec") 1)))

(ert-deftest ert-weed-out-duds ()
  (let ((word-list '("abc" "xbc" "xbx" "xxx")))
    (should (equal (computer-whiz--weed-out-duds "xbc" 2 word-list) '("abc" "xbx")))
    (should (equal (computer-whiz--weed-out-duds "xxx" 0 word-list) '("abc")))
    (should (equal (computer-whiz--weed-out-duds "xxx" 2 word-list) '("xbx")))
    (should (equal (computer-whiz--weed-out-duds "xbx" 1 word-list) '("abc")))))

(ert-deftest ert-mean ()
  (should (= 1.0 (computer-whiz--mean '(1))))
  (should (= 0.0 (computer-whiz--mean '(0))))
  (should (= -1.0 (computer-whiz--mean '(-1))))
  (should (= 2.5 (computer-whiz--mean '(1 2 3 4))))
  (should (= 1.0 (computer-whiz--mean '(1 2 -3 4)))))

(ert-deftest ert-avg-words-eliminated ()
  (let ((word-list '("CONTENT")))
    (should (= 0 (computer-whiz--avg-words-eliminated "CONTENT" word-list))))
  (let ((word-list '("CONTENT" "CONTACT" "LOOTING" "FRAGILE" "XXNTENT")))
    ;; if you guess "CONTENT" first, on average you will eliminate 3.6 words from
    ;; the running.
    (should (= 3.6 (computer-whiz--avg-words-eliminated "CONTENT" word-list)))
    (should (= 4 (computer-whiz--avg-words-eliminated "CONTACT" word-list)))))

(ert-deftest ert-next-best-guess ()
  (let ((word-list '("CONTENT")))
    (should (equal "CONTENT" (computer-whiz--next-best-guess word-list))))
  (let ((word-list '("CONTENT" "CONTACT" "LOOTING" "FRAGILE" "XXNTENT")))
    ;; "CONTACT" always removes 4 words regardless of which password is correct,
    ;; so it is the best guess to start with for this value of word-list.
    (should (equal "CONTACT" (computer-whiz--next-best-guess word-list)))))
