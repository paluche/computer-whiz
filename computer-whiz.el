;;; computer-whiz.el --- Solve Fallout 3/4 terminal hacking puzzles -*- lexical-binding: t -*-

;;; Commentary:
;;
;; This program reads in a list of words from a Fallout 3/4 terminal hacking
;; puzzle (based on the 1970 board game Mastermind) and suggests the optimal
;; sequence of words to guess in order to determine the correct password.
;;
;; It is named for the Fallout 3 perk of the same name.
;;
;; Usage:
;;
;; Do M-x computer-whiz RET in a buffer containing all the (newline separated)
;; passwords provided by the terminal.

;;; Code:

(require 'seq)
(require 'dash)

;;;###autoload
(defun computer-whiz ()
  "Read a list of passwords from a buffer and suggest the best order of guesses.

Buffer should contain one word per line."
  (interactive)
  (let* ((word-list (computer-whiz--read-password-file))
         (result (computer-whiz--suggest-password word-list)))
    (if result
        (message (format "The password must be %s!" result))
      (message "Oops... all passwords ruled out. Try again."))))

(defun computer-whiz--suggest-password (word-list)
  "Recursively suggest guesses from WORD-LIST until we win or run out of words."
  ;; If there are fewer than two words in the list, we've either found the
  ;; answer, or eliminated all words (including the correct password) by mistake.
  (if (> 2 (length word-list))
      (car word-list) ; return the last remaining word or nil.
    ;; there is more than one word in the list, so find the next best guess.
    (let* ((guess (computer-whiz--next-best-guess word-list))
           (result (read-number
                    (format "Try %s. How many matching characters? => " guess)))
           (word-length (length (car word-list))))
      (if (= result word-length)
          guess ; we got it!
        (computer-whiz--suggest-password ; we don't got it. recurse.
         (computer-whiz--weed-out-duds guess result word-list))))))

(defun computer-whiz--next-best-guess (word-list)
  "Determine the next best word to guess from WORD-LIST.
The best word to guess is the word that will rule out the highest
possible number of remaining passwords if it isn't the right
answer."
  (car
   (seq-reduce
    (lambda (best-guess word)
      (let ((eliminated (computer-whiz--avg-words-eliminated word word-list)))
        (if (> eliminated (cdr best-guess))
            (cons word eliminated) ; WORD is our new best guess.
          best-guess)))
    word-list '("" . -1))))

(defun computer-whiz--avg-words-eliminated (word word-list)
  "Determine the average number of words eliminated from WORD-LIST after guessing WORD."
  (computer-whiz--mean
   (mapcar
    (lambda (maybe-correct-word)
      (let* ((matching-characters
              (computer-whiz--matching-characters word maybe-correct-word))
             (new-word-list
              (computer-whiz--weed-out-duds word matching-characters word-list)))
        (- (length word-list) (length new-word-list))))
    word-list)))

(defun computer-whiz--weed-out-duds (input score word-list)
  "Return a subset of WORD-LIST free of words we've ruled out.
We rule out a word if the number of matching characters between
it and INPUT does not equal SCORE."
  (seq-filter (lambda (word)
                (= score (computer-whiz--matching-characters input word)))
              word-list))

(defun computer-whiz--matching-characters (first second)
  "Return the number of matching characters between FIRST and SECOND.
\(Reverse Levenshtein distance?)"
  (apply '+ (-zip-with (lambda (char1 char2) (if (equal char1 char2) 1 0))
                       (split-string first "" t)
                       (split-string second "" t))))

(defun computer-whiz--mean (list)
  "Return the average value of the (numerical) elements of LIST."
  (/ (float (apply '+ list)) (length list)))

(defun computer-whiz--read-password-file ()
  "Return a list of passwords from a buffer, one word per line."
  (split-string (buffer-string) "\n" t))

(provide 'computer-whiz)

;;; computer-whiz.el ends here
